import { Injectable } from '@angular/core';
import { Article } from './article';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { PlatformLocation} from '@angular/common'
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private apiServerUrl = 'http://localhost:8080/api';
  // private apiServerUrl = environment.apiBaseUrl;
  // private apiServerUrl = 'https://server-softray.herokuapp.com/api';

  constructor(private http: HttpClient, platformLocation: PlatformLocation) {
    // this.apiServerUrl = (platformLocation as any).location.origin;

    // if (this.apiServerUrl.includes('localhost')) {
    //   this.apiServerUrl = 'http://localhost:8080'
    // }
   }

  public getArticles(): Observable<Article[]>{
    return this.http.get<Article[]>(`${this.apiServerUrl}/article/all`)
  }
  public addArticles(article: Article): Observable<Article>{
    return this.http.post<Article>(`${this.apiServerUrl}/article/add`, article)
  }

  public updateArticles(article: Article): Observable<Article>{
    return this.http.put<Article>(`${this.apiServerUrl}/article/update`, article)
  }
  public deleteArticles(articleId: number): Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/article/delete/${articleId}`)
  }

}
