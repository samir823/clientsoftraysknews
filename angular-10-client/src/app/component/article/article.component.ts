import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmptyError } from 'rxjs';
import { Article } from 'src/app/_services/article/article';
import { ArticleService } from '../../_services/article/article.service';
import { NgForm } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  public articles: Article[];
  public editarticles: Article;
  public deletearticles: Article;
  public Editor = ClassicEditor;



  constructor(private articleService: ArticleService) { }

  ngOnInit(): void {
    this.getArticles();
  }

  public getArticles(): void {
    this.articleService.getArticles().subscribe(
      (response: Article[]) => {
        this.articles = response
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );

  }

  public onAddEmloyee(addForm: NgForm): void {
    document.getElementById('add-articles-form').click();
    console.log(this.Editor)
    this.articleService.addArticles(addForm.value).subscribe(
      (response: Article) => {
        console.log(response);
        this.getArticles();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateArticle(articles: Article): void {
    this.articleService.updateArticles(articles).subscribe(
      (response: Article) => {
        console.log(response);
        this.getArticles();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteEmloyee(articlesId: number): void {
    this.articleService.deleteArticles(articlesId).subscribe(
      (response: void) => {
        console.log(response);
        this.getArticles();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searcharticless(key: string): void {
    console.log(key);
    const results: Article[] = [];
    for (const article of this.articles) {
      if (article.title.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || article.body.toLowerCase().indexOf(key.toLowerCase()) !== -1)

      {
        results.push(article);
      }
    }
    this.articles = results;
    if (results.length === 0 || !key) {
      this.getArticles();
    }
  }

  public onOpenModal(article: Article, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    console.log(mode + "---"+article);
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addarticlesModal');
    }
    if (mode === 'edit') {
      this.editarticles = article;
      button.setAttribute('data-target', '#updatearticlesModal');
    }
    if (mode === 'delete') {
      this.deletearticles = article;
      button.setAttribute('data-target', '#deletearticlesModal');
    }
    container.appendChild(button);
    button.click();
  }



}

